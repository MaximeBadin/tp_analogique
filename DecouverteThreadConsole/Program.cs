﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecouverteThreadConsole {
    class Program {
        static void Main(string[] args) {

            MyThreadLetter thLetter = new MyThreadLetter();
            thLetter.Start();

            MyThreadNumber thNumber = new MyThreadNumber();
            thNumber.Start(19);

            int dureeMain = 20;
            Console.WriteLine("> Main en route pour {0} secondes ...", dureeMain);

            for(int i = 1; i < dureeMain; i++) {
                Thread.Sleep(1000);
                Console.WriteLine("> Main : {0} sec.", i);
            }

            thLetter.SignalExit.Set();
            thLetter.LaTache.Join();

            thNumber.SignalExit.Set();
            thNumber.LaTache.Join();

            Console.WriteLine("FIN DU PROGRAMME");
            Console.ReadLine();
        }

       
    }
}
