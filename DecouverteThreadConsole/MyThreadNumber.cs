﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecouverteThreadConsole {
    class MyThreadNumber {
        public bool stop = false;
        Random rand = new Random();
        int Number = 1;

        public Thread LaTache;

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public MyThreadNumber() {
            ParameterizedThreadStart delegeateRunLetter = new ParameterizedThreadStart(RunNumber);
            LaTache = new Thread(delegeateRunLetter);
        }

        public void Start(int max = 999) {
            try {
                LaTache.Start(max);
                Console.WriteLine("Letter démmarer correctement ! ");
            } catch (ThreadStateException ex) {
                Console.WriteLine("Letter a déja été démmarer !");
            }
        }


        private void RunNumber(Object Max) {

            while (!SignalExit.WaitOne(rand.Next(100, 1001), false)) {
               
                if(Number < (int)Max) {
                    Number++; 
                } else {
                    Number = 0;
                }

                Console.WriteLine("\t\t > " + Number.ToString());

                Thread.Sleep(rand.Next(323));

            }

        }
    }
}
