﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecouverteThreadConsole {
    public class MyThreadLetter {

        public bool stop = false;
        Random rand = new Random();
        char letter = 'a';
        int compteurVoyelle = 0;

        object verrou = new object();

        public Thread LaTache;
        public Thread compteur;

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public AutoResetEvent SignalVoyelle { get; } = new AutoResetEvent(false); 

        public MyThreadLetter() {
            ThreadStart delegeateRunLetter = new ThreadStart(RunLetter);
            ThreadStart delegeateRunCompteur = new ThreadStart(RunCompteurVoyelle);
            LaTache = new Thread(delegeateRunLetter);
            compteur = new Thread(RunCompteurVoyelle);
        }

        public void Start() {
            try {
                LaTache.Start();
                compteur.Start();
                Console.WriteLine("Letter démmarer correctement ! ");
            } catch (ThreadStateException ex) {
                Console.WriteLine("Letter a déja été démmarer !");
            }
        }
      

        private void RunLetter() {

            Console.WriteLine("\t\t > Le runLetter commence son travail");
            while (!SignalExit.WaitOne(rand.Next(100, 1001), false)) {


                letter = (char)rand.Next(97, 123);
                switch (letter) {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                    case 'y':
                        SignalVoyelle.Set();
                        break;
                    default:
                        break;
                }

                Console.WriteLine("\t\t > Letter : {0}", letter.ToString());

                Thread.Sleep(rand.Next(1267));

            }

            Console.WriteLine("\t\t > Le runLetter a fini son travail");

        }

        private void RunCompteurVoyelle() {
            WaitHandle[] lesSignaux = {SignalExit, SignalVoyelle };
            int indexSignal = -1;
            bool fin = false;

            while (!fin) {
                indexSignal = WaitHandle.WaitAny(lesSignaux);
                switch (indexSignal) {
                    case 1:
                        lock (verrou) {
                            compteurVoyelle++;
                            Console.WriteLine("\t\t Compteur : " + compteurVoyelle.ToString());
                        }
                        break;
                    case 0:
                        fin = true;
                        break;
                }
            }
        }
    }
}
