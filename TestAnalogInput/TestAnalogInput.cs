﻿using MccDaq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestAnalogInput {
    class TestAnalogInput {
        static void Main(string[] args) {
            MccBoard board = new MccBoard(0);

            AnalogInput temp = new AnalogInput(board, 6);
            AnalogInput lum = new AnalogInput(board, 7) ;

            while (true) {
                Console.Clear();
                Console.WriteLine("Temperature : ");
                Console.WriteLine("Brute : " + temp.MesureBrute.ToString());
                Console.WriteLine("Valeur : " + temp.MesureVolt.ToString() + "V");


                Console.WriteLine();

                Console.WriteLine("Lumiere : ");
                Console.WriteLine("Brute : " + lum.MesureBrute.ToString());
                Console.WriteLine("Valeur :" + lum.MesureVolt.ToString() + "V");

                Thread.Sleep(100);
            }
        }
    }
}
