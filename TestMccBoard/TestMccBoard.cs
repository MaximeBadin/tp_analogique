﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MccDaq;

namespace TestMccBoard {
    class TestMccBoard {
        static void Main(string[] args) {
            MccBoard board = new MccBoard(0);

            System.UInt16 DataValue, DataValue2;

            while (true) {
                ErrorInfo error = board.AIn(6, Range.Bip10Volts, out DataValue);
                ErrorInfo error2 = board.AIn(7, Range.Bip10Volts, out DataValue2);


                if (error.Value != 0) {
                    Console.WriteLine("Une erreur est survenue");
                }

                if (error2.Value != 0) {
                    Console.WriteLine("Une erreur est survenue");
                }

                float EngUnits, EngUnits2;
                error = board.ToEngUnits(Range.Bip10Volts, DataValue, out EngUnits);
                error = board.ToEngUnits(Range.Bip10Volts, DataValue2, out EngUnits2);

               
                Console.WriteLine("Temperature");
                Console.WriteLine("Raw data : " + DataValue);
                Console.WriteLine("Data : " + EngUnits);
                Console.WriteLine();

                Console.WriteLine("Luminosité");
                Console.WriteLine("Raw data : " + DataValue);
                Console.WriteLine("Data : " + EngUnits);
                Console.WriteLine();

                Thread.Sleep(1000);

                Console.Clear();
            }
        }
    }
}
