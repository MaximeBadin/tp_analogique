﻿namespace WFAnalogInputThread {
    partial class Ihm {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Eole1Volt = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Eole2Volt = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Eole3Volt = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.setFechTemp = new System.Windows.Forms.Button();
            this.fechTemp = new System.Windows.Forms.TextBox();
            this.TempVolt = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.setFechLum = new System.Windows.Forms.Button();
            this.fechLum = new System.Windows.Forms.TextBox();
            this.LumVolt = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.setFechEole = new System.Windows.Forms.Button();
            this.fechEole = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Eole1Volt);
            this.groupBox1.Location = new System.Drawing.Point(29, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Eole n°1";
            // 
            // Eole1Volt
            // 
            this.Eole1Volt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Eole1Volt.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Eole1Volt.Location = new System.Drawing.Point(6, 16);
            this.Eole1Volt.Name = "Eole1Volt";
            this.Eole1Volt.Size = new System.Drawing.Size(188, 81);
            this.Eole1Volt.TabIndex = 0;
            this.Eole1Volt.Text = "00.00";
            this.Eole1Volt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Eole2Volt);
            this.groupBox2.Location = new System.Drawing.Point(235, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Eole n°2";
            // 
            // Eole2Volt
            // 
            this.Eole2Volt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Eole2Volt.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Eole2Volt.Location = new System.Drawing.Point(6, 16);
            this.Eole2Volt.Name = "Eole2Volt";
            this.Eole2Volt.Size = new System.Drawing.Size(188, 81);
            this.Eole2Volt.TabIndex = 0;
            this.Eole2Volt.Text = "00.00";
            this.Eole2Volt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Eole3Volt);
            this.groupBox3.Location = new System.Drawing.Point(441, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 100);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Eole n°3";
            // 
            // Eole3Volt
            // 
            this.Eole3Volt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Eole3Volt.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Eole3Volt.Location = new System.Drawing.Point(6, 16);
            this.Eole3Volt.Name = "Eole3Volt";
            this.Eole3Volt.Size = new System.Drawing.Size(188, 81);
            this.Eole3Volt.TabIndex = 0;
            this.Eole3Volt.Text = "00.00";
            this.Eole3Volt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.setFechTemp);
            this.groupBox4.Controls.Add(this.fechTemp);
            this.groupBox4.Controls.Add(this.TempVolt);
            this.groupBox4.Location = new System.Drawing.Point(29, 129);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(372, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Température";
            // 
            // setFechTemp
            // 
            this.setFechTemp.Location = new System.Drawing.Point(207, 47);
            this.setFechTemp.Name = "setFechTemp";
            this.setFechTemp.Size = new System.Drawing.Size(159, 47);
            this.setFechTemp.TabIndex = 3;
            this.setFechTemp.Text = "Sauvegarder";
            this.setFechTemp.UseVisualStyleBackColor = true;
            this.setFechTemp.Click += new System.EventHandler(this.setFechTemp_Click);
            // 
            // fechTemp
            // 
            this.fechTemp.Location = new System.Drawing.Point(207, 20);
            this.fechTemp.Name = "fechTemp";
            this.fechTemp.Size = new System.Drawing.Size(159, 20);
            this.fechTemp.TabIndex = 2;
            this.fechTemp.Text = "1000";
            // 
            // TempVolt
            // 
            this.TempVolt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TempVolt.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempVolt.Location = new System.Drawing.Point(12, 13);
            this.TempVolt.Name = "TempVolt";
            this.TempVolt.Size = new System.Drawing.Size(188, 81);
            this.TempVolt.TabIndex = 1;
            this.TempVolt.Text = "00.00";
            this.TempVolt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.setFechLum);
            this.groupBox5.Controls.Add(this.fechLum);
            this.groupBox5.Controls.Add(this.LumVolt);
            this.groupBox5.Location = new System.Drawing.Point(416, 129);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(372, 100);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Luminosité";
            // 
            // setFechLum
            // 
            this.setFechLum.Location = new System.Drawing.Point(207, 47);
            this.setFechLum.Name = "setFechLum";
            this.setFechLum.Size = new System.Drawing.Size(159, 47);
            this.setFechLum.TabIndex = 3;
            this.setFechLum.Text = "Sauvegarder";
            this.setFechLum.UseVisualStyleBackColor = true;
            this.setFechLum.Click += new System.EventHandler(this.setFechLum_Click);
            // 
            // fechLum
            // 
            this.fechLum.Location = new System.Drawing.Point(207, 20);
            this.fechLum.Name = "fechLum";
            this.fechLum.Size = new System.Drawing.Size(159, 20);
            this.fechLum.TabIndex = 2;
            this.fechLum.Text = "100";
            // 
            // LumVolt
            // 
            this.LumVolt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LumVolt.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LumVolt.Location = new System.Drawing.Point(12, 13);
            this.LumVolt.Name = "LumVolt";
            this.LumVolt.Size = new System.Drawing.Size(188, 81);
            this.LumVolt.TabIndex = 1;
            this.LumVolt.Text = "00.00";
            this.LumVolt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.setFechEole);
            this.groupBox6.Controls.Add(this.fechEole);
            this.groupBox6.Location = new System.Drawing.Point(647, 23);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(141, 100);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Période";
            // 
            // setFechEole
            // 
            this.setFechEole.Location = new System.Drawing.Point(6, 46);
            this.setFechEole.Name = "setFechEole";
            this.setFechEole.Size = new System.Drawing.Size(129, 47);
            this.setFechEole.TabIndex = 5;
            this.setFechEole.Text = "Sauvegarder";
            this.setFechEole.UseVisualStyleBackColor = true;
            this.setFechEole.Click += new System.EventHandler(this.setFechEole_Click);
            // 
            // fechEole
            // 
            this.fechEole.Location = new System.Drawing.Point(6, 19);
            this.fechEole.Name = "fechEole";
            this.fechEole.Size = new System.Drawing.Size(129, 20);
            this.fechEole.TabIndex = 4;
            this.fechEole.Text = "250";
            // 
            // Ihm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Ihm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ihm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Eole1Volt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label Eole2Volt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Eole3Volt;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button setFechTemp;
        private System.Windows.Forms.TextBox fechTemp;
        private System.Windows.Forms.Label TempVolt;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button setFechLum;
        private System.Windows.Forms.TextBox fechLum;
        private System.Windows.Forms.Label LumVolt;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button setFechEole;
        private System.Windows.Forms.TextBox fechEole;
    }
}

