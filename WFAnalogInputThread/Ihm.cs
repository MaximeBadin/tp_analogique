﻿using AnalogTools;
using MccDaq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace WFAnalogInputThread {
    public partial class Ihm : Form {

        MccBoard board;
        AnalogThreadInput eole1;
        AnalogThreadInput eole2;
        AnalogThreadInput eole3;
        AnalogThreadInput temp;
        AnalogThreadInput lum;
        DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        public Ihm() {
            InitializeComponent();

            board = new MccBoard(0);

            eole1 = new AnalogThreadInput(board, 0, 250);
            eole1.Start();

            eole2 = new AnalogThreadInput(board, 1, 250);
            eole2.Start();

            eole3 = new AnalogThreadInput(board, 2, 250);
            eole3.Start();

            temp = new AnalogThreadInput(board, 6, 1000, 1);
            temp.Start();

            lum = new AnalogThreadInput(board, 7, 100, 2);
            lum.Start();

            this.timer.Tick += new EventHandler(updateDisplay);
            timer.Start();
        }

        private void updateDisplay(object sender, EventArgs e) {
            this.Eole1Volt.Text = Math.Round(eole1.MesureVolt,2).ToString() + "V";
            this.Eole2Volt.Text = Math.Round(eole2.MesureVolt,2).ToString() + "V";
            this.Eole3Volt.Text = Math.Round(eole3.MesureVolt,2).ToString() + "V";

            this.TempVolt.Text = Math.Round(temp.MesureVolt,0).ToString() + "°C";
            this.LumVolt.Text = Math.Round(lum.MesureVolt,0).ToString() + "%";
        }

        private void setFechTemp_Click(object sender, EventArgs e) {
            int fech = Convert.ToInt32(this.fechTemp.Text);
            this.temp.Fech = fech;
        }

        private void setFechLum_Click(object sender, EventArgs e) {
            int fech = Convert.ToInt32(this.fechLum.Text);
            this.lum.Fech = fech;
        }

        private void setFechEole_Click(object sender, EventArgs e) {
            int fech = Convert.ToInt32(this.fechEole.Text);
            this.eole1.Fech = fech;
            this.eole2.Fech = fech;
            this.eole3.Fech = fech;
        }

        private void Ihm_FormClosing(object sender, FormClosingEventArgs e) {
            this.eole1.Stop();
            this.eole2.Stop();
            this.eole3.Stop();
            this.lum.Stop();
            this.temp.Stop();
        }
    }
}
