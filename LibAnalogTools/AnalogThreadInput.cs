﻿using MccDaq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AnalogTools {
    public class AnalogThreadInput {

        public Thread tache;

        public int chanel;
        private ushort mesureBrute;
        private float mesureVolt;
        private MccBoard board;
        private ErrorInfo error;
        private int fech;

        private int Type;

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);


        public AnalogThreadInput(MccBoard board, int chanel = 0, int fech = 1000, int type = 0) {
            this.chanel = chanel;
            this.board = board;
            this.fech = fech;
            this.Type = type;

            ThreadStart delegateRunMesure = new ThreadStart(RunMesure);
            tache = new Thread(delegateRunMesure);
        }

        public ushort MesureBrute {
            get {
                return this.mesureBrute;
            }
        }

        public float MesureVolt {
            get {
                if (this.Type == 1) {
                    return this.Scale(this.mesureVolt, 0, 5, -20, 54);
                } else if (this.Type == 2) {
                    return this.Scale(this.mesureVolt, 0, 5, 100, 0);
                } else {
                    return this.mesureVolt;
                }
            }
        }

        public int Fech { get => fech; set => fech = value; }

        public void Start() {
            try {
                tache.Start();
            } catch (ThreadStateException ex) {
                throw new ThreadStateException("ERROR : Already started");
            }
        }

        public void RunMesure() {
            while (!SignalExit.WaitOne((int)this.fech, false)) {
                lock (this.board) {
                    this.board.AIn(this.chanel, Range.Bip10Volts, out this.mesureBrute);
                    this.board.ToEngUnits(Range.Bip10Volts, this.MesureBrute, out this.mesureVolt);
                }
            }
        }

        public int Stop() {
            this.SignalExit.Set();
            this.tache.Join();

            return 0;
        }

        private float Scale(float value, int srcMin, int srcMax, int destMin, int destMax) {

            double temp = ((value - srcMin) / (srcMax - srcMin));
            
            double scaled = (temp * (destMax - destMin)) + destMin;

            return (float)scaled;
        }
    }
}
