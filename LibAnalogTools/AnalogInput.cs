﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MccDaq;

namespace AnalogTools {
    public class AnalogInput {
        private int chanel;
        private ushort mesureBrute;
        private float mesureVolt;
        private MccBoard board;
        private ErrorInfo error;

        public AnalogInput(MccBoard board, int chanel = 0) {
            this.chanel = chanel;
            this.board = board;
        }

        public ushort MesureBrute {
            get {
                this.error = this.board.AIn(this.chanel, Range.Bip10Volts, out this.mesureBrute);
                return this.mesureBrute;
            }
        }

        public float MesureVolt {
            get {
                this.error = this.board.ToEngUnits(Range.Bip10Volts, this.MesureBrute, out mesureVolt);
                return this.mesureVolt;
            }
        }
    }
}
