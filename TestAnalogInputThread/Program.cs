﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AnalogTools;
using MccDaq;

namespace TestAnalogInputThread {
    class Program {
        static void Main(string[] args) {

            MccBoard board = new MccBoard(0);

            AnalogThreadInput temp = new AnalogThreadInput(board, 6);
            temp.Start();

            while (true) {
                Console.WriteLine("Brute : " + temp.MesureBrute.ToString());
                Console.WriteLine("Volt : " + temp.MesureVolt.ToString());
                Console.WriteLine();

                Thread.Sleep(500);
            }
        }
    }
}
